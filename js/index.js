$(function(){
        $("[data-toggle='tooltip']").tooltip();
        $("[data-toggle='popover']").popover();
        $(".carousel").carousel({
          interval: 3000
        });
        $("#masInformacion").on("show.bs.modal", function(e){
          console.log('el modal más información se está mostrando');
          $("#masInfo").removeClass('btn-outline-success');
          $("#masInfo").addClass('btn-primary');
          $("#masInfo").prop('disabled', true);
        });
        $("#masInformacion").on("shown.bs.modal", function(e){
          console.log('el modal más información se mostró');
        });
        $("#masInformacion").on("hide.bs.modal", function(e){
          console.log('el modal más información se está ocultando');
        });
        $("#masInformacion").on("hidden.bs.modal", function(e){
          console.log('el modal más información se ocultó');
          $("#masInfo").prop('disabled', false);
          $("#masInfo").removeClass('btn-primary');
          $("#masInfo").addClass('btn-outline-success');
        });
      });